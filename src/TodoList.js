import React, {Component} from 'react';
import TodoItem from "./TodoItem";

class TodoList extends Component{
    constructor(props) {
        super(props);
        this.state={
            items: this.props.items,
        };
    }

    add () {
        let newItemLabel = prompt("Label du nouvel item ?");
        var db = JSON.parse(localStorage.getItem('db'));
        let list = db.findIndex(element => element.name == this.props.name);
        var ajout = {label: newItemLabel, status: ""};
        db[list].items.push(ajout);
        localStorage.setItem('db',JSON.stringify(db));
        document.location.reload();
    }

    remove () {
        var db = JSON.parse(localStorage.getItem('db'));
        let list = db.findIndex(element => element.name == this.props.name);
        db.splice(list, 1);
        localStorage.setItem('db',JSON.stringify(db));
        document.location.reload();
    }

    render() {
        let {name}=this.state;
        var db = JSON.parse(localStorage.getItem('db'));
        let list = db.findIndex(element => element.name == this.props.name);

        return (
            <div className="todo-list" class="list">
                <h2>{this.props.name} <button class="delete-button" onClick = {this.remove.bind(this)}>x</button></h2>
                {this.state.items.map((item) => <TodoItem index={list} item={item} label={item.label} status={item.status}/>)}
                <button class="add-button" onClick = {this.add.bind(this)}>+</button>
            </div>
        );
    }
}
export default TodoList;