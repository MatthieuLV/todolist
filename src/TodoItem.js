import React, {Component} from 'react';

class TodoItem extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            label: this.props.label,
            status: this.props.status,
            item: this.props.item,
            index: this.props.index,
        };
    }

    remove () {
        let index = this.props.index;
        var db = JSON.parse(localStorage.getItem('db'));
        let list = db[index].items.findIndex(element => element.label == this.props.label);
        db[index].items.splice(list, 1);
        localStorage.setItem('db',JSON.stringify(db));
        document.location.reload();
    }

    check () {
        let check;
        let index = this.props.index;
        var db = JSON.parse(localStorage.getItem('db'));
        let list = db[index].items.findIndex(element => element.label == this.props.label);
        if (document.getElementById(this.props.label).checked == true) {
            check = "checked";
        } else {
            check = "";
        }
        db[index].items[list].status = check;
        localStorage.setItem('db',JSON.stringify(db));
        document.location.reload();
    }

    render() {
        let {label} = this.state;

        return (
            <div>
                <input type="checkbox" id={label} defaultChecked={this.state.status} onChange={this.check.bind(this)} />
                <label for={label}>{label}</label>
                <button class='delete-button' onClick = {this.remove.bind(this)}>x</button>
            </div>
        );

    }
}
export default TodoItem;