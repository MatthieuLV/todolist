import React, { Component } from 'react';
import './App.css';
import TodoList from './TodoList.js';

class App extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            lists: JSON.parse(localStorage.getItem('db'))
        }
    }

    render() {
        const add = () =>{
            if (this.state.lists == undefined){
                let newListName = prompt("Nom de la nouvelle liste ?");
                var newList = [{name: newListName, items: []}];
                localStorage.setItem('db',JSON.stringify(newList));
                document.location.reload();
            } else {
                let newListName = prompt("Nom de la nouvelle liste ?");
                var db = JSON.parse(localStorage.getItem('db'));
                var newList = {name: newListName, items: []};
                db.push(newList);
                localStorage.setItem('db',JSON.stringify(db));
                document.location.reload();
            }

        }

        if (this.state.lists == undefined){
            return (
                <div className="App">
                    <div id="app">
                        <div>
                            <ul>
                                <button class="add add-button" onClick = {add}>+</button>
                            </ul>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="App">
                    <div id="app">
                        <div>
                            <ul>
                                <button class="add add-button" onClick = {add}>Ajouter une liste</button>
                                {this.state.lists.map((list) => <TodoList name={list.name} items={list.items}/>)}
                            </ul>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default App;
